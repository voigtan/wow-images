const download = require("./download.js");
var fs = require('fs');
const path = "./wow/";

const createFolder = folder => {
    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }
}

var dirs = [
    './cache',
    './charimages',
    path
];

// Create folders
dirs.forEach(createFolder);

//
download("http://wow.zamimg.com/images/wow/icons/small/class_warrior.jpg", path + "c_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_paladin.jpg", path + "c_2.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_hunter.jpg", path + "c_3.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_rogue.jpg", path + "c_4.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_priest.jpg", path + "c_5.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_deathknight.jpg", path + "c_6.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_shaman.jpg", path + "c_7.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_mage.jpg", path + "c_8.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_warlock.jpg", path + "c_9.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_monk.jpg", path + "c_10.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_druid.jpg", path + "c_11.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/class_demonhunter.jpg", path + "c_12.jpg");

//
download("http://wow.zamimg.com/images/wow/icons/small/race_human_male.jpg", path + "r_1_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_human_female.jpg", path + "r_1_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_orc_male.jpg", path + "r_2_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_orc_female.jpg", path + "r_2_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_dwarf_male.jpg", path + "r_3_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_dwarf_female.jpg", path + "r_3_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_nightelf_male.jpg", path + "r_4_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_nightelf_female.jpg", path + "r_4_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_scourge_male.jpg", path + "r_5_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_scourge_female.jpg", path + "r_5_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_tauren_male.jpg", path + "r_6_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_tauren_female.jpg", path + "r_6_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_gnome_male.jpg", path + "r_7_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_gnome_female.jpg", path + "r_7_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_troll_male.jpg", path + "r_8_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_troll_female.jpg", path + "r_8_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_goblin_male.jpg", path + "r_9_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_goblin_female.jpg", path + "r_9_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_bloodelf_male.jpg", path + "r_10_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_bloodelf_female.jpg", path + "r_10_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_draenei_male.jpg", path + "r_11_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_draenei_female.jpg", path + "r_11_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_worgen_male.jpg", path + "r_22_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_worgen_female.jpg", path + "r_22_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_male.jpg", path + "r_24_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_female.jpg", path + "r_24_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_male.jpg", path + "r_25_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_female.jpg", path + "r_25_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_male.jpg", path + "r_26_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/race_pandaren_female.jpg", path + "r_26_1.jpg");

//
download("http://wow.zamimg.com/images/wow/icons/small/ability_warrior_savageblow.jpg", path + "t_1_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_warrior_innerrage.jpg", path + "t_1_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_warrior_defensivestance.jpg", path + "t_1_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_holy_holybolt.jpg", path + "t_2_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_paladin_shieldofthetemplar.jpg", path + "t_2_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_holy_auraoflight.jpg", path + "t_2_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/ability_hunter_bestialdiscipline.jpg", path + "t_3_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_hunter_focusedaim.jpg", path + "t_3_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_hunter_camouflage.jpg", path + "t_3_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/ability_rogue_eviscerate.jpg", path + "t_4_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_backstab.jpg", path + "t_4_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_stealth.jpg", path + "t_4_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_holy_powerwordshield.jpg", path + "t_5_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_holy_guardianspirit.jpg", path + "t_5_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_shadow_shadowwordpain.jpg", path + "t_5_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_deathknight_bloodpresence.jpg", path + "t_6_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_deathknight_frostpresence.jpg", path + "t_6_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_deathknight_unholypresence.jpg", path + "t_6_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_nature_lightning.jpg", path + "t_7_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_nature_lightningshield.jpg", path + "t_7_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_nature_magicimmunity.jpg", path + "t_7_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_holy_magicalsentry.jpg", path + "t_8_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_fire_firebolt02.jpg", path + "t_8_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_frost_frostbolt02.jpg", path + "t_8_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_shadow_deathcoil.jpg", path + "t_9_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_shadow_metamorphosis.jpg", path + "t_9_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_shadow_rainoffire.jpg", path + "t_9_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/monk_stance_drunkenox.jpg", path + "t_10_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/monk_stance_wiseserpent.jpg", path + "t_10_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/monk_stance_whitetiger.jpg", path + "t_10_2.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/spell_nature_starfall.jpg", path + "t_11_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_druid_catform.jpg", path + "t_11_1.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_racial_bearform.jpg", path + "t_11_2.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/spell_nature_healingtouch.jpg", path + "t_11_3.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/ability_demonhunter_specdps.jpg", path + "t_12_0.jpg");
download("http://wow.zamimg.com/images/wow/icons/small/ability_demonhunter_spectank.jpg", path + "t_12_1.jpg");

download("http://wow.zamimg.com/images/wow/icons/small/inv_misc_questionmark.jpg", path + "t_0.jpg");
