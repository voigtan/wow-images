var Jimp = require("jimp");
const imageWidth = 18;
const border = 1;

const reflect = (promise) => promise.then((v) => v, (e) => null);

var isAlliance = (race) => [1, 3, 4, 7, 11, 22, 25].indexOf(race) !== -1 ? "Alliance" : null;
var isHorde = (race) => [2, 5, 6, 8, 9, 10, 26].indexOf(race) !== -1 ? "Horde" : null;

const getFaction = (race) => isAlliance(race) || isHorde(race) || "Neutral";

const createFactionBackground = (faction, level) => {
    const race = getFaction(faction);

    return new Promise((resolve, reject) => {
        var c = Jimp.rgbaToInt(255, 255, 0, 255);

        if (race === "Horde") {
            c = Jimp.rgbaToInt(255, 0, 0, 255);
        } else if (race === "Alliance") {
            c = Jimp.rgbaToInt(0, 0, 255, 255);
        }


        var bannerSize = imageWidth * 2;

        new Jimp(bannerSize, bannerSize, c, function (err, image) {
            if (err) {
                reject(err);
            } else {
                Jimp
                    .loadFont(Jimp.FONT_SANS_16_WHITE).then((font) => {
                        var lvl = level.toString();
                        var numWidth = lvl.split('').map((c) => {
                            var char = font.chars[c];

                            return (c == "1" ? 6 : char.width) + char.xoffset;
                        }).reduce((x, y) => x + y, 0);
                        var x = (bannerSize - 18) / 2;
                        var y = Math.floor((bannerSize - (numWidth + font.info.padding[0])) / 2);
                        image.print(font, y, x, lvl, bannerSize);
                    }).then(() => resolve(image));
            }
        });
    });
}

const createCharacter = (data, width, border) => {
    return new Promise((resolve, reject) => {
        var _height = (border * (3 + 1)) + (width * 3);
        var _width = border * 2 + width;
        var c = Jimp.rgbaToInt(0, 0, 0, 255);

        var img = new Jimp(_width, _height, c, function (err, avatarImage) {
            if (err) {
                reject(err);
            } else {
                var res = (image, index) => avatarImage.blit(image.resize(width, width), border, border + (index * (border + width)));

                Promise.all([
                    createFactionBackground(data.race, data.level).then((img) => res(img, 0)),
                    Jimp.read("./wow/c_" + data.class + ".jpg").then((img) => res(img, 1)),
                    Jimp.read("wow/r_" + data.race + "_" + data.gender + ".jpg").then((img) => res(img, 2))
                ]).then(() => resolve(avatarImage));

            }
        });
    });

}


const createCharactersImage = (arr, toon, filename) => {

    return new Promise((resolve, reject) => {
        Promise
            .all(arr)
            .then((results) => {
                // Filter out any character that did not get an error

                var success = results.filter(x => x !== null);

                var avatarSize = toon ? (imageWidth + border) * 3 : 0;

                const height = (imageWidth + border) * 3 + border;
                const width = (border + imageWidth) * success.length + border + avatarSize;


                (new Jimp(width, height, 0x00000000, function (err, _image) {
                    if (toon !== null) {
                        index = avatarSize;
                    }

                    var chars = success.map((x, index) => {
                        return createCharacter(x, imageWidth, border).then((img) => {
                            _image.blit(img, avatarSize + index * (imageWidth + border), 0)
                        })
                    });

                    Promise
                        .all(chars)
                        .then(() => {
                            return Jimp.read("charImages/" + success[0].name + "_" + success[0].realm + ".jpg")
                            .then((img) =>
                                _image.blit(img.resize(avatarSize, avatarSize), 0, 0)
                            )
                        })
                        .then(() => {
                            _image.write(filename + ".png");
                            console.log("[IMAGE] - Created");
                        });
                }));
            });
    });
}

module.exports = createCharactersImage;