const request = require('request');
const fetch = require('node-fetch');
const fs = require('fs');

const download = require("./download");

const getFile = (name, realm, location) => `./cache/${name}_${realm}_${location}.json`;
const apikey = "m7ec9xtrvn6p3fj8bpndt978um8m78g6";

/**
 * Get a local cached version of the JSON response from live servers
 * @param {string} name - The Character name
 * @param {string} realm - The Realm you want to look up
 * @param {string} location - Zone (EU/US/CN)
 */
const readFromDisk = function (name, realm, location) {
    return new Promise(function (resolve, reject) {
        fs.readFile(getFile(name, realm, location), 'utf8', (err, data) => {
            if (err) {
                reject();
            } else {
                resolve(JSON.parse(data));
            }
        });
    });
}

/**
 * Get Character Information from live servers
 * @param {string} name - The Character name
 * @param {string} realm - The Realm you want to look up
 * @param {string} location - Zone (EU/US/CN)
 */
const apiFetch = (name, realm, location) => {
    var url = (location.toUpperCase() == "CN" ?
        "https://api.battlenet.com.cn/wow/character/" :
        "https://" + location + ".api.battle.net/wow/character/");

    url = url + realm + "/" + encodeURIComponent(name) + "?fields=talents&locale=en_GB&apikey=" + apikey;

    return fetch(url)
        .then((response) => response.json())
        .then((data, reject) => {
            if (data.status) {
                return Promise.reject(new Error("Couldn't find " + name + " - " + realm + " - " + location));
            } else {
                return Promise.resolve(data);
            }
        });
}



const getPortrait = (name, realm, location, thumbnail) => {
    const file = "./charImages/" + name + "_" + realm + ".jpg";

    if (!fs.existsSync(file)) {
        download("http://render-" + location + ".worldofwarcraft.com/character/" + thumbnail, file);
    } else {
        console.log("[CHARACTER IMAGE] - file exists, dont download it");
    }
}

/**
 * 
 * @param {string} name - The Character name
 * @param {string} realm - The Realm you want to look up
 * @param {string} location - Zone (EU/US/CN)
 */
var _getChar = function (name, realm, location) {

    var getSpec = (data) => {
        var selected = data.talents.filter((v) => v.selected)[0];
        var talent = selected.spec.order;
        data._talent = talent;

        return data;
    };

    const saveCharacterToDisk = (data) => {
        if (data && data.name) {
            fs.writeFile(getFile.call(this, name, realm, location), JSON.stringify(data), 'utf8');
            return data;
        } else {
            reject(new Error("[CHARACTER INFORMATION] Couldn't find " + name + " - " + realm + " - " + location));
        }
    };

    return new Promise((resolve, reject) => {
        readFromDisk(name, realm, location)
            .then(data => {
                console.log("[CHARACTER INFORMATION] Got From Disk");
                return data;
            })
            .catch(() => {
                console.log("[CHARACTER INFORMATION] did not get from cache");
                return apiFetch(name, realm, location)
                    .then(saveCharacterToDisk)
                    .catch(reject);
            })
            .then(getSpec)
            // download portrait
            .then((data) => {
                getPortrait(data.name, data.realm, location, data.thumbnail);
                resolve(data);
            });
    });
}

module.exports = _getChar;