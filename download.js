var fs = require('fs');
var request = require('request');

module.exports = function (uri, filename, callback) {
    request.head(uri, function (err, res, body) {
        var _cb = () => console.log(filename, "done");
        var cb = callback || _cb;

        request(uri).pipe(fs.createWriteStream(filename)).on('close', cb);
    });
};